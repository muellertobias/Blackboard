﻿using Blackboard.Core.Entities;
using System.Collections.Generic;

namespace Blackboard.Core.Gateways
{
    public interface IRepository<TEntity> where TEntity : EntitiyBase
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetBy(ulong id);
        bool Insert(TEntity entity);
        void Delete(ulong id);
        bool Update(TEntity entity);
    }
}
