﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.Entities;
using Blackboard.Core.Gateways;
using System;

namespace Blackboard.Core.UseCases.DetachMessage
{
    class DetachMessageInteractor : IInputBoundary<DetachMessageRequest, IDetachMessageResponse>
    {
        private readonly IRepository<Message> messageRepository;
        private readonly IOutputBoundary<IDetachMessageResponse> outputBoundary;

        public DetachMessageInteractor(IOutputBoundary<IDetachMessageResponse> outputBoundary, IRepository<Message> messageRepository)
        {
            this.outputBoundary = outputBoundary ?? throw new ArgumentNullException(nameof(outputBoundary));
            this.messageRepository = messageRepository ?? throw new ArgumentNullException(nameof(messageRepository));
        }

        public void Execute(DetachMessageRequest request)
        {
            if (request.ID == 0UL)
            {
                outputBoundary.Handle(new DetachMessagesFailedResponse());
            }
            else
            {
                var message = messageRepository.GetBy(request.ID);
                if (message == null)
                {
                    outputBoundary.Handle(new DetachMessagesFailedResponse());
                }
                else
                {
                    messageRepository.Delete(request.ID);
                    outputBoundary.Handle(new DetachMessageSuccessfullyResponse());
                }
            }
        }
    }
}
