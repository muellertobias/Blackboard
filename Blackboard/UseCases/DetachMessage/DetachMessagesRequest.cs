﻿using Blackboard.Core.Boundaries;
namespace Blackboard.Core.UseCases.DetachMessage
{
    public class DetachMessageRequest : IRequest<IDetachMessageResponse>
    {
        public ulong ID { get; set; }
    }
}
