﻿using Blackboard.Core.Boundaries;

namespace Blackboard.Core.UseCases.GetMessageByID
{
    public class GetMessageByIDRequest : IRequest<IGetMessageResponse>
    {
        public ulong ID { get; set; }
    }
}
