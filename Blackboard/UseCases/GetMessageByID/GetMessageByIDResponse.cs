﻿namespace Blackboard.Core.UseCases.GetMessageByID
{
    public class GetMessageByIDResponse : IGetMessageResponse
    {
        public MessageResponseModel Message { get; set; }
    }
}
