﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.Entities;
using Blackboard.Core.Gateways;
using System;


namespace Blackboard.Core.UseCases.GetMessageByID
{
    class GetMessageByIDInteractor : IInputBoundary<GetMessageByIDRequest, IGetMessageResponse>
    {
        private readonly IRepository<Message> messageRepository;
        private readonly IOutputBoundary<IGetMessageResponse> outputBoundary;

        public GetMessageByIDInteractor(IOutputBoundary<IGetMessageResponse> outputBoundary, IRepository<Message> messageRepository)
        {
            this.outputBoundary = outputBoundary ?? throw new ArgumentNullException(nameof(outputBoundary));
            this.messageRepository = messageRepository ?? throw new ArgumentNullException(nameof(messageRepository));
        }

        public void Execute(GetMessageByIDRequest request)
        {
            Message message = messageRepository.GetBy(request.ID);

            if (message == null)
            {
                outputBoundary.Handle(new MessageNotFoundResponse());
            }
            else
            {
                var responseModel = new MessageResponseModel()
                {
                    ID = message.ID,
                    Text = message.Text,
                    CreationTime = message.CreationTime
                };
                outputBoundary.Handle(new GetMessageByIDResponse() { Message = responseModel });
            }
        }
    }
}
