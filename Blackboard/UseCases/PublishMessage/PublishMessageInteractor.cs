﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.Entities;
using Blackboard.Core.Gateways;
using System;

namespace Blackboard.Core.UseCases.PublishMessage
{
    class PublishMessageInteractor : IInputBoundary<PublishMessagesRequest, IPublishMessagesResponse>
    {
        private readonly IRepository<Message> messageRepository;
        private readonly IOutputBoundary<IPublishMessagesResponse> outputBoundary;

        public PublishMessageInteractor(IOutputBoundary<IPublishMessagesResponse> outputBoundary, IRepository<Message> messageRepository)
        {
            this.outputBoundary = outputBoundary ?? throw new ArgumentNullException(nameof(outputBoundary));
            this.messageRepository = messageRepository ?? throw new ArgumentNullException(nameof(messageRepository));
        }

        public void Execute(PublishMessagesRequest request)
        {
            if (!IsValid(request.Text))
            {
                outputBoundary.Handle(new PublishMessagesFailedResponse());
            }

            Message message = new Message(request.Text);
            bool success = messageRepository.Insert(message);

            if (success)
                outputBoundary.Handle(new PublishMessagesSuccessfullyResponse());
            else
                outputBoundary.Handle(new PublishMessagesFailedResponse());
        }

        #region Validation
        private bool IsValid(string text)
        {
            return !string.IsNullOrEmpty(text);
        }
        #endregion
    }
}
