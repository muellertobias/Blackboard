﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.UseCases.PublishMessage;

namespace Blackboard.Core.UseCases.PublishMessage
{
    public class PublishMessagesRequest : IRequest<IPublishMessagesResponse>
    {
        public string Text { get; set; }
    }
}
