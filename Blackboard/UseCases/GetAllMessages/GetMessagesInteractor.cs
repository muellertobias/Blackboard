﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.Entities;
using Blackboard.Core.Gateways;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Blackboard.Core.UseCases.GetAllMessage
{
    class GetAllMessagesInteractor : IInputBoundary<GetAllMessagesRequest, GetAllMessagesResponse>
    {
        private readonly IRepository<Message> messageRepository;
        private readonly IOutputBoundary<GetAllMessagesResponse> outputBoundary;

        public GetAllMessagesInteractor(IOutputBoundary<GetAllMessagesResponse> outputBoundary, IRepository<Message> messageRepository)
        {
            this.outputBoundary = outputBoundary ?? throw new ArgumentNullException(nameof(outputBoundary));
            this.messageRepository = messageRepository ?? throw new ArgumentNullException(nameof(messageRepository));
        }

        public void Execute(GetAllMessagesRequest request)
        {
            IEnumerable<Message> messages = messageRepository.GetAll();

            var responseModels = messages.Select(msg => new MessageResponseModel
            {
                ID = msg.ID,
                CreationTime = msg.CreationTime,
                Text = msg.Text
            });

            outputBoundary.Handle(new GetAllMessagesResponse() { Messages = responseModels });
        }
    }
}
