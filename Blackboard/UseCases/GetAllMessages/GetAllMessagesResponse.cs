﻿using System.Collections.Generic;

namespace Blackboard.Core.UseCases.GetAllMessage
{
    public class GetAllMessagesResponse
    {
        public IEnumerable<MessageResponseModel> Messages { get; set; }
    }
}
