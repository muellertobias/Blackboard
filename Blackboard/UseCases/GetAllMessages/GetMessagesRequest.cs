﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.UseCases.GetAllMessage;

namespace Blackboard.Core.UseCases.GetAllMessage
{
    public class GetAllMessagesRequest : IRequest<GetAllMessagesResponse>
    {
    }
}
