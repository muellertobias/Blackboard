﻿using System;

namespace Blackboard.Core.UseCases
{
    public struct MessageResponseModel
    {
        public ulong ID { get; set; }
        public DateTime CreationTime { get; set; }
        public string Text { get; set; }
    }
}
