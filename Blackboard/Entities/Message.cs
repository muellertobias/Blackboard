﻿using System;

namespace Blackboard.Core.Entities
{
    public class Message : EntitiyBase
    {
        public DateTime CreationTime { get; private set; }
        public string Text { get; set; }

        public Message(string text)
           : this(text, DateTime.Now)
        {
        }

        public Message(string text, DateTime creationTime)
            : base()
        {
            Text = text;
            CreationTime = creationTime;
        }
    }
}
