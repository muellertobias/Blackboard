﻿using System;

namespace Blackboard.Core.Entities
{
    public abstract class EntitiyBase
    {
        public ulong ID { get; private set; }

        public EntitiyBase()
        {
            Guid guid = Guid.NewGuid();
            var bytes = guid.ToByteArray();

            ulong id = 0;

            for (int i = 0; i < 8; i++)
            {
                id |= (ulong)bytes[i] << (8 * i);
            }

            ID = id;
        }
    }
}
