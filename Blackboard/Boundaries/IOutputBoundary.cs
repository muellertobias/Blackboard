﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackboard.Core.Boundaries
{
    public interface IOutputBoundary<TResponse>
    {
        void Handle(TResponse response);
    }
}
