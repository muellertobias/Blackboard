﻿
namespace Blackboard.Core.Boundaries
{
    public interface IInputBoundary<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        void Execute(TRequest request);
    }
}
