﻿using Blackboard.Core.Entities;
using Blackboard.Core.Gateways;

namespace Blackboard.Core.Factory
{
    public class Settings
    {
        public IRepository<Message> Messages { get; set; }
    }
}