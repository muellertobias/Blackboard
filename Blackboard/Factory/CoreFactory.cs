﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.UseCases.DetachMessage;
using Blackboard.Core.UseCases.GetAllMessage;
using Blackboard.Core.UseCases.GetMessageByID;
using Blackboard.Core.UseCases.PublishMessage;

namespace Blackboard.Core.Factory
{
    public class CoreFactory : ICoreFactory
    {
        private readonly Settings settings;

        public CoreFactory(Settings settings)
        {
            this.settings = settings;
        }

        public IInputBoundary<GetAllMessagesRequest, GetAllMessagesResponse> Build(IOutputBoundary<GetAllMessagesResponse> output)
        {
            return new GetAllMessagesInteractor(output, settings.Messages);
        }

        public IInputBoundary<GetMessageByIDRequest, IGetMessageResponse> Build(IOutputBoundary<IGetMessageResponse> output)
        {
            return new GetMessageByIDInteractor(output, settings.Messages);
        }

        public IInputBoundary<PublishMessagesRequest, IPublishMessagesResponse> Build(IOutputBoundary<IPublishMessagesResponse> output)
        {
            return new PublishMessageInteractor(output, settings.Messages);
        }

        public IInputBoundary<DetachMessageRequest, IDetachMessageResponse> Build(IOutputBoundary<IDetachMessageResponse> output)
        {
            return new DetachMessageInteractor(output, settings.Messages);
        }
    }
}
