﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.UseCases.DetachMessage;
using Blackboard.Core.UseCases.GetAllMessage;
using Blackboard.Core.UseCases.GetMessageByID;
using Blackboard.Core.UseCases.PublishMessage;

namespace Blackboard.Core.Factory
{
    public interface ICoreFactory
    {
        IInputBoundary<GetAllMessagesRequest, GetAllMessagesResponse> Build(IOutputBoundary<GetAllMessagesResponse> output);
        IInputBoundary<GetMessageByIDRequest, IGetMessageResponse> Build(IOutputBoundary<IGetMessageResponse> output);
        IInputBoundary<PublishMessagesRequest, IPublishMessagesResponse> Build(IOutputBoundary<IPublishMessagesResponse> output);
        IInputBoundary<DetachMessageRequest, IDetachMessageResponse> Build(IOutputBoundary<IDetachMessageResponse> output);
    }
}