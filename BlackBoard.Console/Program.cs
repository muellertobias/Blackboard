﻿using Blackboard.Core.Factory;
using BlackBoard.Console.Handlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackBoard.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            TextReader reader = System.Console.In;
            TextWriter output = System.Console.Out;

            Settings settings = new Settings() { Messages = new InMemoryMessageRepository() };
            var factory = new CoreFactory(settings);

            MessagePublisher publisher = new MessagePublisher(output, factory);
            MessagePrinter printer = new MessagePrinter(output, factory);

            bool isRunning = true;
            while (isRunning)
            {
                output.Write("> ");
                string input = reader.ReadLine();
                switch (input)
                {
                    case "send":
                        output.Write("Text: ");
                        string text = reader.ReadLine();
                        publisher.Publish(text);
                        break;
                    case "show":
                        printer.Print();
                        break;
                    case "exit":
                        isRunning = false;
                        break;
                    default:
                        output.WriteLine("send / show / exit");
                        break;
                }
            }
        }
    }
}
