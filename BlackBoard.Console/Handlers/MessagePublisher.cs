﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.Factory;
using Blackboard.Core.UseCases.PublishMessage;
using System.IO;

namespace BlackBoard.Console.Handlers
{
    class MessagePublisher : IOutputBoundary<IPublishMessagesResponse>
    {
        private TextWriter output;
        private IInputBoundary<PublishMessagesRequest, IPublishMessagesResponse> publicMessageUseCase;

        public MessagePublisher(TextWriter output, ICoreFactory factory)
        {
            this.output = output;
            this.publicMessageUseCase = factory.Build(this);
        }

        public void Publish(string text)
        {
            publicMessageUseCase.Execute(new PublishMessagesRequest() { Text = text });
        }

        public void Handle(IPublishMessagesResponse response)
        {
            if (response is PublishMessagesFailedResponse)
                Handle(response as PublishMessagesFailedResponse);

            if (response is PublishMessagesSuccessfullyResponse)
                Handle(response as PublishMessagesSuccessfullyResponse);
        }

        private void Handle(PublishMessagesFailedResponse response)
        {
            output.WriteLine("Pubish Message: fail");
        }

        private void Handle(PublishMessagesSuccessfullyResponse response)
        {
            output.WriteLine("Pubish Message: successfully");
        }
    }
}
