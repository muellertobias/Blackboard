﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.Factory;
using Blackboard.Core.UseCases.GetAllMessage;
using System.IO;
using System.Linq;

namespace BlackBoard.Console.Handlers
{
    class MessagePrinter : IOutputBoundary<GetAllMessagesResponse>
    {
        private TextWriter output;
        private IInputBoundary<GetAllMessagesRequest, GetAllMessagesResponse> useCase;

        public MessagePrinter(TextWriter output, ICoreFactory factory)
        {
            this.output = output;
            this.useCase = factory.Build(this);
        }

        public void Handle(GetAllMessagesResponse response)
        {
            var messages = response.Messages;

            if (messages?.Count() == 0)
            {
                output.WriteLine("No Messages available!");
            }

            output.WriteLine("----------");
            foreach (var msg in messages)
            {
                output.WriteLine("{0}: {1} ({2})", msg.ID, msg.Text, msg.CreationTime);
                output.WriteLine("----------");
            }
        }

        public void Print()
        {
            useCase.Execute(new GetAllMessagesRequest());
        }
    }
}
