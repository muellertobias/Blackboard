﻿using Blackboard.Core.Entities;
using Blackboard.Core.Gateways;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackBoard.Console
{
    class InMemoryMessageRepository : IRepository<Message>
    {
        private Dictionary<ulong, Message> messages;

        public InMemoryMessageRepository()
        {
            messages = new Dictionary<ulong, Message>();
        }

        public void Delete(ulong id)
        {
            messages.Remove(id);
        }

        public IEnumerable<Message> GetAll()
        {
            return messages.Values.AsEnumerable();
        }

        public Message GetBy(ulong id)
        {
            if (messages.ContainsKey(id))
            {
                return messages[id];
            }
            return null;
        }

        public bool Insert(Message entity)
        {
            if (messages.ContainsKey(entity.ID))
                return false;

            messages.Add(entity.ID, entity);
            return true;
        }

        public bool Update(Message entity)
        {
            if (messages.ContainsKey(entity.ID))
            {
                messages[entity.ID].Text = entity.Text;
                return true;
            }
            return false;
        }
    }
}
