﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Blackboard.Web.Models;
using Microsoft.AspNetCore.Http;
using Blackboard.Core.Boundaries;
using Blackboard.Core.Factory;
using BlackBoard.Web.Repository;
using Blackboard.Core.UseCases.GetAllMessage;

namespace Blackboard.Web.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(ICoreFactory factory) : base()
        {
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
