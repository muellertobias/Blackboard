﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.Factory;
using Blackboard.Core.UseCases;
using Blackboard.Core.UseCases.DetachMessage;
using Blackboard.Core.UseCases.GetAllMessage;
using Blackboard.Core.UseCases.GetMessageByID;
using Blackboard.Core.UseCases.PublishMessage;
using Blackboard.Web.Models;
using Blackboard.Web.Presenters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace Blackboard.Web.Controllers
{
    public class MessageController : Controller
    {
        private readonly SingleMessagePresenter messagePresenter;
        private readonly MultipleMessagePresenter messagesPresenter;
        private readonly MessageDetachingPresenter messageDetachingPresenter;

        private readonly IInputBoundary<GetAllMessagesRequest, GetAllMessagesResponse> getAllMessagesUseCase;
        private readonly IInputBoundary<GetMessageByIDRequest, IGetMessageResponse> getMessageByIDUseCase;
        private readonly IInputBoundary<DetachMessageRequest, IDetachMessageResponse> detachMessageUseCase;


        public MessageController(ICoreFactory factory) : base()
        {
            messagePresenter = new SingleMessagePresenter();
            messagesPresenter = new MultipleMessagePresenter();
            messageDetachingPresenter = new MessageDetachingPresenter();

            getAllMessagesUseCase = factory.Build(messagesPresenter);
            getMessageByIDUseCase = factory.Build(messagePresenter);
            detachMessageUseCase = factory.Build(messageDetachingPresenter);
        }

        // GET: Message
        public IActionResult Index()
        {
            getAllMessagesUseCase.Execute(new GetAllMessagesRequest());
            return View(messagesPresenter.GetMessageViewModels());
        }

        // GET: Message/Delete/5
        public ActionResult Delete(ulong id)
        {
            getMessageByIDUseCase.Execute(new GetMessageByIDRequest() { ID = id });
            var messageViewModel = messagePresenter.GetMessageViewModel();
            if (messageViewModel != null)
                return View(messageViewModel);

            return RedirectToAction(nameof(Index));
        }

        // POST: Message/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(ulong id, IFormCollection collection)
        {
            try
            {
                detachMessageUseCase.Execute(new DetachMessageRequest() { ID = id });
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        } 
    }
}