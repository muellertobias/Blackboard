﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blackboard.Core.Boundaries;
using Blackboard.Core.Factory;
using Blackboard.Core.UseCases.PublishMessage;
using Blackboard.Web.Presenters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Blackboard.Web.Controllers
{
    public class PublishingController : Controller
    {
        private readonly IInputBoundary<PublishMessagesRequest, IPublishMessagesResponse> publishMessageUseCase;
        private readonly MessagePublishingPresenter presenter;


        public PublishingController(ICoreFactory factory)
        {
            presenter = new MessagePublishingPresenter();
            publishMessageUseCase = factory.Build(presenter);
        }
        // GET: Message/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Message/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            var text = collection["Text"];
            publishMessageUseCase.Execute(new PublishMessagesRequest() { Text = text });

            if (presenter.LastMessageWasSuccessfullyCreated)
                return RedirectToAction("Index", "Message");
            else
                return View();
        }
    }
}