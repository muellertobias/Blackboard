﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.UseCases.PublishMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blackboard.Web.Presenters
{
    class MessagePublishingPresenter : IOutputBoundary<IPublishMessagesResponse>
    {
        public bool LastMessageWasSuccessfullyCreated { get; private set; }

        public void Handle(IPublishMessagesResponse response)
        {
            if (response is PublishMessagesSuccessfullyResponse)
                LastMessageWasSuccessfullyCreated = true;
            else
                LastMessageWasSuccessfullyCreated = false;
        }
    }
}
