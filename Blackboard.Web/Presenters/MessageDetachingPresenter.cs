﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.UseCases.DetachMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blackboard.Web.Presenters
{
    class MessageDetachingPresenter : IOutputBoundary<IDetachMessageResponse>
    {
        public void Handle(IDetachMessageResponse response)
        {

        }
    }
}
