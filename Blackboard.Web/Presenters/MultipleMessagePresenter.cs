﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.UseCases;
using Blackboard.Core.UseCases.GetAllMessage;
using Blackboard.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blackboard.Web.Presenters
{
    class MultipleMessagePresenter : IOutputBoundary<GetAllMessagesResponse>
    {
        private List<MessageViewModel> messageViewModels;

        public MultipleMessagePresenter()
        {
            messageViewModels = new List<MessageViewModel>();
        }

        public void Handle(GetAllMessagesResponse response)
        {
            var messages = response.Messages;
            var viewModels = new List<MessageViewModel>();
            foreach (var message in messages)
            {
                viewModels.Add(CreateMessageViewModel(message));
            }

            messageViewModels = viewModels;
        }

        internal List<MessageViewModel> GetMessageViewModels()
        {
            return messageViewModels;
        }

        private MessageViewModel CreateMessageViewModel(MessageResponseModel responseModel)
        {
            return new MessageViewModel(responseModel.ID, responseModel.Text, responseModel.CreationTime);
        }
    }
}
