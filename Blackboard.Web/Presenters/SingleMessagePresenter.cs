﻿using Blackboard.Core.Boundaries;
using Blackboard.Core.UseCases;
using Blackboard.Core.UseCases.GetAllMessage;
using Blackboard.Core.UseCases.GetMessageByID;
using Blackboard.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blackboard.Web.Presenters
{
    class SingleMessagePresenter : IOutputBoundary<IGetMessageResponse>
    {
        private MessageViewModel messageViewModel;

        public SingleMessagePresenter()
        {
            messageViewModel = null;
        }

        public void Handle(IGetMessageResponse response)
        {
            if (response is GetMessageByIDResponse)
                Handle(response as GetMessageByIDResponse);

            if (response is MessageNotFoundResponse)
                Handle(response as MessageNotFoundResponse);
        }

        private void Handle(GetMessageByIDResponse response)
        {
            var responseModel = response.Message;
            var viewModel = CreateMessageViewModel(responseModel);

            messageViewModel = viewModel;
        }

        private void Handle(MessageNotFoundResponse response)
        {
        }

        public MessageViewModel GetMessageViewModel()
        {
            return messageViewModel;
        }

        private MessageViewModel CreateMessageViewModel(MessageResponseModel responseModel)
        {
            return new MessageViewModel(responseModel.ID, responseModel.Text, responseModel.CreationTime);
        }
    }
}
