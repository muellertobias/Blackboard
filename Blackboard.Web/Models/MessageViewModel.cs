﻿using System;

namespace Blackboard.Web.Models
{
    public class MessageViewModel
    {
        public ulong ID { get; set; }
        public string Text { get; set; }
        public DateTime CreationTime { get; set; }

        public MessageViewModel()
        { 
        }

        public MessageViewModel(ulong id, string text, DateTime creationTime)
        {
            ID = id;
            Text = text;
            CreationTime = creationTime;
        }
    }
}
